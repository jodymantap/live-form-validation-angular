import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-sasstest',
  templateUrl: './sasstest.component.html',
  styleUrls: ['./sasstest.component.sass']
})
export class SasstestComponent implements OnInit, OnDestroy {
  isAuthenticated = false;
  private userSub: Subscription;
  token:string = localStorage.getItem('token');

  constructor(private authService: AuthService, private router: Router) { }

  onLogout() {
    localStorage.removeItem('userData');
    window.location.reload();
  }

  isOpened:Boolean = true;
  navToggle() {
    this.isOpened = !this.isOpened;
  }

  ngOnInit() {
    this.userSub = this.authService.user.subscribe(user => {
      this.isAuthenticated = !!user;
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

}
