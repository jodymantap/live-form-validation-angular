import { Component, NgZone, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.sass"],
})
export class RegisterComponent implements OnInit {
  emailRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  name: string;
  email: string;
  password: string;
  emailVal: string = "Invalid email address.";
  isEmailWrong: boolean = false;
  passwordVal: string = "Password too short.";
  isPasswordWrong: boolean = false;
  nameCheck: boolean = false;
  emailCheck: boolean = false;
  passwordCheck: boolean = false;
  isLoading: boolean = false;
  isAuthenticated = false;
  private userSub: Subscription;


  constructor(private router: Router, private authService: AuthService, private zone: NgZone) {}

  onValidateName(event: Event) {
    if ((<HTMLInputElement>event.target).value.length > 0) {
      this.nameCheck = true;
    } else this.nameCheck = false;
  }

  onValidateEmail(event: Event) {
    if (
      this.emailRegex.test((<HTMLInputElement>event.target).value) &&
      (<HTMLInputElement>event.target).value.length > 0
    ) {
      this.isEmailWrong = false;
      this.emailCheck = true;
    } else if (
      !this.emailRegex.test((<HTMLInputElement>event.target).value) &&
      (<HTMLInputElement>event.target).value.length > 0
    ) {
      this.isEmailWrong = true;
      this.emailCheck = false;
    } else {
      this.isEmailWrong = false;
      this.emailCheck = false;
    }
  }

  onValidatePassword(event: Event) {
    if (
      (<HTMLInputElement>event.target).value.length < 8 &&
      (<HTMLInputElement>event.target).value.length > 0
    ) {
      this.isPasswordWrong = true;
      this.passwordCheck = false;
    } else if (
      (<HTMLInputElement>event.target).value.length >= 8 &&
      (<HTMLInputElement>event.target).value.length > 0
    ) {
      this.isPasswordWrong = false;
      this.passwordCheck = true;
    } else {
      this.isPasswordWrong = false;
      this.passwordCheck = false;
    }
  }

  onRegister() {
    this.isLoading = true;
    this.authService.signup(this.name, this.email, this.password).subscribe(
      (res: any) => {
        console.log(res);
        this.isLoading = false;
        this.router.navigate(['']);
      },
      (error) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  ngOnInit() {}
}
