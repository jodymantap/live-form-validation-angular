import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Subject } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { User } from "./user.model";
import { Router } from "@angular/router";

interface AuthResponseData {
  name: string;
  email: string;
  access_token: string;
}

@Injectable({ providedIn: "root" })
export class AuthService {
  user = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, private router: Router) {}

  handleError;

  signup(name: string, email: string, password: string) {
    return this.http
      .post<AuthResponseData>("http://localhost:5000/api/register", {
        name: name,
        email: email,
        password: password,
      })
      .pipe(
        catchError(this.handleError),
        tap((res) => {
          console.log("This is a res", res);
          const user = new User(res.data.email, res.data.access_token);
          this.user.next(user);
          localStorage.setItem("userData", JSON.stringify(user));
        })
      );
  }

  autologin() {
    const userData: {
      email: string;
      _access_token: string;
    } = JSON.parse(localStorage.getItem("userData"));
    if (!userData) {
      return;
    }

    const loadedUser = new User(userData.email, userData._access_token);

    if (loadedUser.access_token) {
      this.user.next(loadedUser);
    }
  }
}
