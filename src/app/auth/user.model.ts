export class User {
  constructor(public email: string, private _access_token: string) {}

  get access_token() {
    return this._access_token;
  }
}
