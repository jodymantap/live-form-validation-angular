import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./auth/auth.guard";
import { RegisterComponent } from "./auth/register/register.component";
import { FeedComponent } from "./pages/feed/feed.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/feed",
    pathMatch: "full",
  },
  {
    path: "feed",
    component: FeedComponent,
    canActivate: [AuthGuard],
  },
  { path: "register", component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
